# My development machine

Install [ansible] >= 2.10 and run

```bash
make
```

If you run into errors, you can better debug them with

```bash
make verbose
```

It should work on any Ubuntu-like distribution, and also on macOS by running:

```bash
make mac
```

[ansible]: https://docs.ansible.com/ansible/index.html
