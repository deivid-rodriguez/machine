default:
	ansible-playbook -i hosts machine.yml --ask-become-pass

verbose:
	ansible-playbook -i hosts machine.yml --ask-become-pass -vvv

mac:
	ansible-playbook -i hosts machine.yml --skip-tags sudo

%:
	ansible-playbook -i hosts machine.yml --ask-become-pass --tags=$@
